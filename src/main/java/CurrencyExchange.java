import java.math.BigDecimal;
import java.util.Map;

public class CurrencyExchange {

    private Map<String, BigDecimal> accounts;
    private Map<String, BigDecimal> rates;


    public CurrencyExchange(Map<String, BigDecimal> startUpValues, Map<String, BigDecimal> rates) {

        this.accounts = startUpValues;
        this.rates = rates;
    }


    public Map<String, BigDecimal> accounts() {
        return this.accounts;
    }

    public BigDecimal getRateFor(String currency) {
        return this.rates.get(currency);
    }

    public void buy(String currency, BigDecimal value) {
        accounts.put(currency, accounts.get(currency).add(value));
    }

    public void sell(String currency, BigDecimal value) {
        BigDecimal actual = accounts.get(currency);
        BigDecimal after = actual.subtract(value);

        if (after.compareTo(new BigDecimal(0)) < 0) {
            throw new RuntimeException("Currency Exchange service is temporary unavailable. Try again later.");
        }

        accounts.put(currency, after);
    }
}
