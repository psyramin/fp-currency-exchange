import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class ExchangeService {
    CurrencyExchange curencyExchange;
    User user;

    public ExchangeService(CurrencyExchange curencyExchange, User user) {
        this.curencyExchange = curencyExchange;
        this.user = user;
    }

    public void exchangeFromPLN(String currency, BigDecimal value) {

        BigDecimal rate = this.curencyExchange.getRateFor(currency);

        BigDecimal afterExchange = value.multiply(rate, MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_EVEN);

        this.curencyExchange.buy("PLN", afterExchange);
        this.user.sell("PLN", afterExchange);
        this.curencyExchange.sell(currency, value);
        this.user.buy(currency, value);
    }

    public void exchangeToPLN(String currency, BigDecimal value) {

        BigDecimal rate = curencyExchange.getRateFor(currency);

        BigDecimal afterExchange = value.multiply(rate, MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_EVEN);

        this.curencyExchange.buy(currency, value);
        this.user.sell(currency,value);
        this.curencyExchange.sell("PLN", afterExchange);
        this.user.buy("PLN",afterExchange);
    }

}
