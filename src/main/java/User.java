import java.math.BigDecimal;
import java.util.Map;

public class User {
    private Map<String, BigDecimal> accounts;
    private Integer id;

    public User(Map<String, BigDecimal> accounts, Integer id) {
        this.accounts = accounts;
        this.id = id;
    }

    public Map<String, BigDecimal> accounts() {
        return accounts;
    }

    public Integer id() {
        return id;
    }

    public void buy(String currency, BigDecimal value) {

        accounts.put(currency, accounts.get(currency).add(value));
    }

    public void sell(String currency, BigDecimal value) {

        BigDecimal actual = accounts.get(currency);
        BigDecimal after = actual.subtract(value);

        if (after.compareTo(new BigDecimal(0)) < 0) {
            throw new RuntimeException(String.format("Not enough money on %s account to perform this operation.", currency));
        }

        accounts.put(currency, after);
    }
}
