import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Map<String, BigDecimal> startUpValues = new HashMap<>();
        Map<String, BigDecimal> exchangeRates = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            if (0 == i % 3) {
                startUpValues.put(args[i], new BigDecimal(args[i + 1]));
                exchangeRates.put(args[i], new BigDecimal(args[i + 2]));
            }
        }

        CurrencyExchange curencyExchange = new CurrencyExchange(startUpValues, exchangeRates);

        Map<String, BigDecimal> accounts = new HashMap<>();
        accounts.put("PLN", new BigDecimal(2000));
        accounts.put("EUR", new BigDecimal(2000));
        accounts.put("USD", new BigDecimal(2000));
        User user = new User(accounts, 1);

        ExchangeService exchangeService = new ExchangeService(curencyExchange, user);

        Scanner scanner = new Scanner(System.in);

        boolean running = true;

        while (running) {
            String input = scanner.nextLine();
            if (":q".equalsIgnoreCase(input) || "exit".equalsIgnoreCase(input)) {
                running = false;
                System.out.println("Thank you for using our service. Good bye!");
            }

            if ("help".equalsIgnoreCase(input) || "-h".equals(input)){
                printHelp();
            }

            listenToAccountListing(user, input);

            try {
                listenToExchangeCommands(exchangeService, input);
            } catch (Exception e) {
                System.out.print("Invalid command for exchange operation. Use the following format: [buy|sell] [amount] [currency]");
            }
        }

    }

    private static void printHelp(){
        System.out.println(String.format("%s %s", "account", "shows all user accounts balance."));
        System.out.println(String.format("%s %s", "buy [amount] [currency]", "buys currency You pick."));
        System.out.println(String.format("%s %s", "sell [amount] [currency]", "sells currency You pick."));
        System.out.println(String.format("%s %s", "exit || :q", "closes app."));
    }

    private static void listenToAccountListing(User user, String input) {
        if ("account".equalsIgnoreCase(input)) {

            for (Map.Entry<String, BigDecimal> entry : user.accounts().entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());
            }
        }
    }

    private static void listenToExchangeCommands(ExchangeService exchangeService, String input) {

        String[] commandParts = input.split("\\s+");
        String buyOrSell = commandParts[0];

        if (buyOrSell.equalsIgnoreCase("BUY")) {

            BigDecimal amount = new BigDecimal(commandParts[1]);
            String currency = commandParts[2].toUpperCase();

            try {
                exchangeService.exchangeFromPLN(currency, amount);
                printSuccessOperation(buyOrSell, currency, amount);
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }

        } else if (buyOrSell.equalsIgnoreCase("SELL")) {

            BigDecimal amount = new BigDecimal(commandParts[1]);
            String currency = commandParts[2].toUpperCase();
            try {
                exchangeService.exchangeToPLN(currency, amount);
                printSuccessOperation(buyOrSell, currency, amount);
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }
        }
    }

    private static void printSuccessOperation(String operation, String currency, BigDecimal amount) {
        String buySell = operation.equalsIgnoreCase("buy") ? "bought" : "sold";
        System.out.println(String.format("You have successfully %s %s %s. ", buySell, amount, currency));
    }
}
