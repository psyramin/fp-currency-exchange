#Overview

This is a proof of concept of currency exchange platform.
This application is a very simple command line app.

##Run app
javac Main.java
java Main args 
#### start up arguments
...[currency] [amount] [rate]

On application start-up the exchange service currencies, amounts and rates are being passed as an arguments.
So for example to start-up a service that is handling EUR and USD exchanges please provide the following command:
``java Main EUR 5000 4.31 USD 5000 3.72 PLN 5000 1``
Note that PLN also needs to be provided.

On app start-up the hard coded user will be created. Users currencies and amounts are:

| Currency | Initial amount |
| :---: | :---: |
| EUR | 2000 | 
| USD| 2000 | 
| PLN| 2000 | 

For available commands: run ``help`` or ``-h``
 
